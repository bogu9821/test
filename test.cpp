
#include "Arduino.h"

//#include <vector>

template<size_t N>
struct Register_Size_Type
{
};

template<>
struct Register_Size_Type<8>
{
    using Type = std::uint8_t;
};

template<>
struct Register_Size_Type<16>
{
    using Type = std::uint16_t;
};

template<>
struct Register_Size_Type<32>
{
    using Type = std::uint32_t;
};



struct Reg_ReadOnly
{
};

struct Reg_WriteOnly
{
};

struct Reg_ReadWrite : Reg_ReadOnly, Reg_WriteOnly
{
};

template<size_t N, typename Tag = Reg_ReadWrite>
class Register
{
public:
   using Type = typename Register_Size_Type<N>::Type;

    explicit Register(const std::uintptr_t t_address)
        : m_ptr(reinterpret_cast<Type*>(t_address))
    {

    }

   template<typename std::enable_if<std::is_base_of<Reg_ReadOnly, Tag>::value,
             bool>::type = true>
    operator Type() const
    {
        return *m_ptr;
    }


    Type& operator[](const size_t t_bit)
    {
        return (*m_ptr & (1 << t_bit)) ? 1 : 0;
    }


private:
   volatile Type* m_ptr;
};

//

void setup()
{
    Register<32> c{0x222};
    
    auto f = static_cast<std::uint32_t>(c);
    
}

void loop()
{
    
}